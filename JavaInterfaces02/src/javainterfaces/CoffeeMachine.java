/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javainterfaces;

/**
 *
 * @author human
 */
public interface CoffeeMachine {

    public static final int VOLUME_ESSPRESSO = 40;
    int VOLUME_AMERICANO = 150;

    /**
     *
     * @param type
     * @return
     */
    public Coffee makeCoffee(String type);

    /**
     * 
     * @param type
     * @param sugar
     * @return 
     */
    public Coffee makeCoffee(String type, int sugar);

}
