/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javainterfaces;

import java.util.Scanner;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Saeco cm_saeco1 = new Saeco();
//        Saeco cm_saeco2 = new Saeco();

        Coffee esspresso1 = cm_saeco1.makeCoffee("esspresso", 0);
//        System.out.println(esspresso1);

//        Coffee esspresso2 = cm_saeco1.makeCoffee("esspresso", 0);
//        System.out.println(esspresso2);
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.println("Select coffee type:");
            System.out.println("1. [E]sspresso");
            System.out.println("2. [A]mericano");
            System.out.println("0. [C]ancel");

            String choose = scan.nextLine();

            Coffee coffee = null;

            switch (choose.toLowerCase()) {
                case "1":
                case "e":
                    coffee = cm_saeco1.makeCoffee("esspresso");
                    break;
                case "2":
                case "a":
                    coffee = cm_saeco1.makeCoffee("americano");
                    break;
                case "0":
                case "c":
                    // NOOP
                    break;
                default:
                    System.out.println("WRONG CHOISE");
                    break;
            }

            if (coffee != null) {
                System.out.println("Sugar? ('0' default)");
                String input = scan.nextLine();

                if (!input.isEmpty()) {
                    int sugar = Integer.parseInt(input);
                    coffee.setSugar(sugar);
                }
                
                System.out.println("Take your coffee: " + coffee);
            }
        }
    }

}
