/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javainterfaces;

/**
 *
 * @author human
 */
public class Saeco implements CoffeeMachine {

    @Override
    public Coffee makeCoffee(String type, int sugar) {
        Coffee coffee = null;

        switch (type.toLowerCase()) {
            case "esspresso":
                coffee = new Coffee(CoffeeMachine.VOLUME_ESSPRESSO);
                coffee.setSugar(sugar);
                break;
            case "americano":
                coffee = new Coffee(CoffeeMachine.VOLUME_AMERICANO);
                coffee.setSugar(sugar);
                break;
        }

        return coffee;
    }

    @Override
    public Coffee makeCoffee(String type) {
        Coffee makeCoffee = makeCoffee(type, 0);
        return makeCoffee;
    }

}
