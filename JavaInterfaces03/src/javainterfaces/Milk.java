/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javainterfaces;

/**
 *
 * @author human
 */
public class Milk extends Drink {
    
    private String type;

    public Milk(String type, int volume) {
        super(volume);
        this.type = type;
    }

    public String getType() {
        return type;
    }
    
}
